﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.ViewModels
{
    public class BMIData
    {
        [Required(ErrorMessage = "Required")]
        [Range(30,50, ErrorMessage = "請輸入範圍30-50")]
        [Display(Name ="體重")]
         public float Weight { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(50, 250, ErrorMessage = "請輸入範圍50-250")]
        [Display(Name = "身高")]
        public float Height { get; set; }

        public float? BMI { get; set; }
        public string Level { get; set; }
    }

}