﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Login()
        {
            TempData["msg"] = "我是從你旁邊送過來的!";
            return RedirectToAction("Index");

        }

        public ActionResult Index()
        {
            return View();
        }
    }
}