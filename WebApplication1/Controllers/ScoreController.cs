﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(float score)
        {
           
            string level = "";

            if (0 <= score && score < 20)
            {
                level = "E";
            }
            else if (20 <= score && score < 40)
            {
                level = "D";
            }
            else if (40 <= score && score < 60)
            {
                level = "C";
            }
            else if (60 <= score && score < 80)
            {
                level = "B";
            }
            else if (80 <= score && score <=     100)
            {
                level = "A";
            }
            
            ViewBag.Score = score;
            ViewBag.level = level;

            return View();
        }
    }
}