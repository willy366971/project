﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    [MetadataType(typeof(UsersMetadata))]
    public partial class Users
    {
        
    }

    public class UsersMetadata
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display (Name="Name")]
        [StringLength(5)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display (Name="Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display (Name="Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display (Name="Birthday")]
        public System.DateTime Birthday { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display (Name="Gender")]
        public bool Gender { get; set; }
    }
}